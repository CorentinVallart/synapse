﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable()]
    class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;

        private List<Mission> _mission;


        public decimal MargeBruteCourante()
        {
            decimal resultat = 0;

            resultat = _prixFactureMO - CumulCoutMo();

            return resultat;
        }



        private decimal CumulCoutMo()
        {
            decimal resultat = 0;

            foreach (Mission missionCourante in _mission)
            {
                resultat = resultat + (missionCourante.NbHeuresEffectues() * missionCourante.Executant.tauxHoraire);
            }

            return resultat;
        }

    }
}
