﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{

    [Serializable()]
    class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrévues;
        private Dictionary<DateTime, int> _relevéHoraire;

        private Intervenant _executant;

        public Intervenant Executant
        {
            get
            {
                return _executant;
            }
        }

        public Dictionary<DateTime, int> RelevéHoraire
        {
            get
            {
                return _relevéHoraire;
            }
        }

        public void AjouteReleve(DateTime date, int nbHeures)
        {
            _relevéHoraire.Add(date, nbHeures);
        }

        public int NbHeuresEffectues()
        {
            int resultat = 0;

            foreach (KeyValuePair<DateTime, int> i in _relevéHoraire)
            {

                resultat = resultat + i.Value;
            }

            return resultat;
        }

    }
}
