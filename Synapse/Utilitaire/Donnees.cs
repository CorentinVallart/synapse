﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metier;

namespace Synapse.Utilitaire
{
    class Donnees
    {
        private static List<Projet> _collectionProjet;
        private static List<Intervenant> _collectionIntervenant;


        public static List<Projet> CollectionProjet
        {
            get
            {
                if (_collectionProjet == null)
                {
                    _collectionProjet = (List<Projet>)Persistances.ChargerDonnees("Projet");
                    if (_collectionProjet == null)
                        _collectionProjet = new List<Projet>();
                }
                return Donnees._collectionProjet;
            }
            set { Donnees._collectionProjet = value; }
        }


        public static List<Intervenant> CollectionIntervenant
        {
            get
            {
                if (_collectionIntervenant == null)
                {
                    _collectionIntervenant = (List<Intervenant>)Persistances.ChargerDonnees("Intervenant");
                    if (_collectionIntervenant == null)
                        _collectionIntervenant = new List<Intervenant>();
                }
                return Donnees._collectionIntervenant;
            }
            set { Donnees._collectionIntervenant = value; }
        }
    }
}
